﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25Generic_HW
{
    class Knight: IComparable<Knight>, IgetName
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string Title { get; private set; }

        public Knight(string name, string birthTown, string title)
        {
            Name = name;
            BirthTown = birthTown;
            Title = title;
        }

        public string this[string txt]
        {
            get
            {
                if (txt == Name)
                    return Name;
                if (BirthTown == txt)
                    return BirthTown;
                if (Title == txt)
                    return Title;
                return "Unknown";
            }
            set
            {
                if (txt == Name)
                    Name = value;
                if (BirthTown == txt)
                    BirthTown = value;
                if (Title == txt)
                    Title = value;
            }
        }
        public override string ToString()
        {
            return $"Name: {Name} Brith HomeTown: {BirthTown} Title: {Title}";
        }

        public int CompareTo(Knight other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
