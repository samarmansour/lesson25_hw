﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25Generic_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            Knight knight = new Knight("Zoru","Mexico","Lan Masa 245");
            Knight knight1 = new Knight("Black", "USA", "LA 245");
            Knight knight2= new Knight("Stephano", "Italy", "Roma 2536");
            Magicain magicain = new Magicain("Elf","USA","Las Vegas");
            RoundTable<Knight> knights = new RoundTable<Knight>();
            RoundTable<Magicain> magicains = new RoundTable<Magicain>();

            knights.Add(knight);
            knights.Add(knight1);
            knights.Add(knight2);
            foreach (var item in knights)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("==============================================");
            knights.RemoveAt(0);
            knights.InsertAt(4, new Knight("Roben hood", "Spain", "Malaga 15"));
            knights.Sort();
            knights.GetRounded(10);

            foreach (var item in knights)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("==============================================");

            Console.WriteLine(knight["Nimo"]);
            Console.WriteLine($"string indexer: {magicain["Elf"]}");
            Console.WriteLine($"Indexer by int: {knights[1]}");
            
            
            magicains.Add(magicain);

            foreach (var item in knights)
            {
                Console.WriteLine(item);
            }
        }
    }
}
