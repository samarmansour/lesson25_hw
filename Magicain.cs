﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25Generic_HW
{
    class Magicain: IComparable<Magicain>, IgetName
    {
        public string Name { get; private set; }
        public string BirthTown { get; private set; }
        public string FavoriteSpell { get; private set; }

        public Magicain(string name, string birthTown, string favoriteSpell)
        {
            Name = name;
            BirthTown = birthTown;
            FavoriteSpell = favoriteSpell;
        }

        public string this[string txt]
        {
            get
            {
                if (txt == Name)
                    return Name;
                if (BirthTown == txt)
                    return BirthTown;
                if (FavoriteSpell == txt)
                    return FavoriteSpell;
                return "Unknown";
            }
            set
            {

                if (txt == Name)
                    Name = value;
                if (BirthTown == txt)
                    BirthTown = value;
                if (FavoriteSpell == txt)
                    FavoriteSpell = value;
               
            }
        }
        public override string ToString()
        {
            return $"Name: {this.Name} Brith HomeTown: {this.BirthTown} Favorite Spell: {this.FavoriteSpell}";
        }

        public int CompareTo(Magicain other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
