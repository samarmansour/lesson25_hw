﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson25Generic_HW
{
    class RoundTable<T> : IEnumerable<T> where T : IgetName 
    {
        private List<T> entities;

        public RoundTable()
        {
            entities = new List<T>();
        }

        public void Add(T newItem)
        {
            entities.Add(newItem);
        }

        public void RemoveAt(int num)
        {
            if (num == entities.Count)
                return;
            entities.RemoveAt(num % entities.Count);

        }

        public void Clear()
        {
            entities.Clear();
        }

        public void InsertAt(int index, T item)
        {
            entities.Insert(index % entities.Count, item);
        }

        public void Sort()
        {
            entities.Sort();
        }

        public List<T> GetRounded(int len)
        {
            List<T> result = new List<T>();
            int count = 0;
            int index = 0;
            while (count++ < len)
            {
                result.Add(entities[index++]);
                if (index >= entities.Count)
                    index = 0;
            }
            return result;
        }

        public T this[int index]
        {
            get
            {
                return entities[index % entities.Count];
            }
        }

        public T this[string name]
        {
            get
            {
                foreach (T item in entities)
                {
                    if (item.Name == name)
                    {
                        return item;
                    }
                }
                return default(T);
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return entities.GetEnumerator();
        }
    }
}
